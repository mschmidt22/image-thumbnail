from PIL import Image

import os

marco = Image.open('marco/marco.png')
frame_size = marco.size
imagelist = [f for f in os.listdir('./images_input/') if not f.startswith('.')]


def combinar(imagen):
    """
    Asume que imagen tiene RGBA.
    """
    if imagen.size != marco.size:
        imagen = imagen.resize(frame_size)
        print(
            f'Se modificó el tamaño de la imagen para coincidir con la del marco ({marco.size})'
        )
    try:
        imagen_nueva = Image.alpha_composite(imagen, marco)
    except Exception as e:
        print(f'No pudimos combinar la imagen con el marco porque {e}')
        return None
    return imagen_nueva


for image in imagelist:
    file_ = "./images_input/" + image
    img = Image.open(file_)
    if img.mode != 'RGBA':
        print(
            f'La imagen tenía el modo {img.mode} pero necesitamos RGBA; intentaremos cambiarlo.'
        )
        try:
            img = img.convert('RGBA')
        except Exception as e:
            print(
                f'No pudimos pasarlo al modo apropiado porque {e}; no procesamos esta imagen.'
            )
            continue
    new_image = combinar(img)
    if new_image is None:
        continue
    # new_image.show()
    nombre = ".".join(image.split('.')[:-1] + ['png'])
    new_image.save("images_output/" + nombre)
