# My project's README

0) instalar pillow: 
```
$ pip install pillow
```

1)descargar el repo poniendo en la terminal:
```
$ git clone https://mschmidt22@bitbucket.org/mschmidt22/image-thumbnail.git
```

2) colocar las imagenes a enmarcar en la carpeta images_input

3) ejecutar desde la carpeta principal python script.py:
```
$ python script.py
```

4) buscar las imagenes enmarcadas en la carpeta images_output

5) si sse quiere cambiar el marco reemplazarlo en la carpeta marco, con el nombre marco.png
